#! /bin/sh

gcloud compute --project=gitlab-db-benchmarking instances get-serial-port-output patroni-data-analytics-01-db-db-benchmarking --zone=us-east1-c --port=1 2>&1 | grep 'Chef Client finished'

